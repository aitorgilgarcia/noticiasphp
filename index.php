<?php 
//Nos aseguramos que SIEMPRE estamos conectados a la bbdd
require('includes/conexion.php');

//Le decimos qué página tiene que cargar
if (isset($_GET['p'])) {
  $p=$_GET['p'];
}else{
  $p='productos.php';
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gestor de productos en PHP - Mysql</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  </head>
  <body>
    
    <section class="container">
      
      <?php require('paginas/'.$p); ?> <!-- Cambiamos productos.php por .$p (p es página) -->

    </section>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php 
//Desconectar de la base de datos
mysqli_close($conexion);
 ?>