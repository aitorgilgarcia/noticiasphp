<h2>
	Alta de producto -
	<small>
		Insertar un nuevo producto
	</small>
	-
	<small>
		<a href="index.php">Cancelar</a>
	</small>
</h2>
<br>

<form action="index.php?p=insercion.php" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group"> <!--enctype (voy a mandarle datos en forma de ficheros) -->

		<label for="nombre">Nombre:</label>
		<input type="text" name="nombre" id="nombre" class="form-control">

		<label for="precio">Precio:</label>
		<input type="text" name="precio" id="precio" class="form-control">

		<label for="descripcion">Descripción:</label>
		<textarea name="descripcion" id="descripcion" cols="30" rows="3" class="form-control"></textarea>

		<label for="imagen">Imagen:</label>
		<input type="file" name="imagen" id="imagen" class="form-control">


		<input type="submit" value="Alta producto" name="insertar" class="btn btn-default">
	</div>
</form>