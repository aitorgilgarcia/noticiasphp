<h2>Listado de productos
  <small>
    <a href="index.php?p=insertar.php"> <!-- Al pinchar cargara insertar.php -->
        Dar de alta un nuevo producto
    </a>
  </small>
</h2>
<?php 
//Pensar la pregunta que quiero hacer
$sql="SELECT * FROM productos ORDER BY fecha DESC"; //Seleccioname todo de la tabla productos, ordenalos por fecha descendente

//Realizamos la pregunta
$consulta=mysqli_query($conexion, $sql); //Todos los productos de la tabla son mostrados
		
		//Hay que trabajar de una fila en una fila->
//Analizamos la respuesta
/*$fila=mysqli_fetch_array($conexion); // Te muestra la primera fila y automaticamente pasa a la siguiente posicion -> Creamos un while*/
while($fila=mysqli_fetch_array($consulta)){
	?>								
	<article>
			<header>
				<h2>
					<?php echo $fila['nombre']; ?>
					-
					<small>
						<a href="index.php?p=borrar.php&id=<?php echo $fila['id'];?>" onClick="if(!confirm('Estas seguro?')){return false;};">Borrar</a>

						<a href="index.php?p=modificar.php&id=<?php echo $fila['id'];?>">Modificar</a>
					</small>
				</h2>
			</header>
		<section>
		<img src="imagenes/productos/<?php echo $fila['imagen'];?>" 
		width="100" style="float:left;">
			<?php echo $fila['descripcion']; ?>			
		</section>
	</article>
	<div style="clear: both;"></div>
	<hr>
<?php
}
?>
