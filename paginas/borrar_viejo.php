<h2>
	Borrar producto -
	<small>
		Borramos producto
	</small>
</h2>
<br>
<?php 
//Recogemos el id de producto que queremos borrar
$id=$_GET['id'];

//Borramos la imagen FISICAMENTE del directorio 
$sql="SELECT * FROM productos WHERE id=$id"; //Selecciono el producto que quiero
$consulta=mysqli_query($conexion, $sql); //Ejecutamos la consulta
$fila=mysqli_fetch_array($consulta); //Extraigo mi unica fila
if ($fila['imagen']!='ninguna.png'){ //Si esa imagen es distinto de ninguna 
	unlink('imagenes/productos/'.$fila['imagen']); //Eliminamos imagen
}

//Pensamos la pregunta a la base de datos
$sql="DELETE FROM productos WHERE id=$id";

//Realizamos la pregunta
$consulta=mysqli_query($conexion,$sql); //Siempre será la misma pregunta (la consulta que yo hago a sql) devolverán o true o false ( si se ha ejecutado bien o no)

if ($consulta==true) {
	echo 'Consulta realizada con exito';
	header('Location:index.php?p=productos.php');
}else{
	echo '<br><hr>Error de consulta';
}
?>
