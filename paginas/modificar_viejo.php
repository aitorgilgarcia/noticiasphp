<?php 
//tengo que obtener los datos del producto que quiero modificar
//Recojo el ed del producto que quiero modificar
$id=$_GET['id'];

//Pienso la pregunta
$sql="SELECT * FROM productos WHERE id=$id";

//Ejecuto la consulta
$consulta=mysqli_query($conexion, $sql);

//Extraigo ese único producto
$fila=mysqli_fetch_array($consulta); //Como solo tengo uno extraigo la fila que quiero modificar
 ?>
<h2>
	Modificar producto -
	<small>
		Modificar un producto
	</small>
	-
	<small>
		<a href="index.php">Cancelar</a>
	</small>
</h2>
<br>

<form action="index.php?p=modificacion.php" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">

		<label for="nombre">Nombre:</label>
		<input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo $fila['nombre'];?>">

		<label for="precio">Precio:</label>
		<input type="text" name="precio" id="precio" class="form-control" value="<?php echo $fila['precio'];?>">

		<label for="descripcion">Descripción:</label>
		<textarea name="descripcion" id="descripcion" cols="30" rows="3" class="form-control"><?php echo $fila['descripcion'];?></textarea> <!-- El text area no tiene value -->

		<br>
		<div class="col-md-1">
			<img src="imagenes/productos/<?php echo $fila['imagen'];?>" width="100"><br>
		</div>
		
		<div class="col-md-11">
			<label for="imagen">Imagen:</label>
			<input type="file" name="imagen" id="imagen" class="form-control">		
		</div>

		<br>

		<input type="hidden" name="imagen_actual" value="<?php echo $fila['imagen'];?>"> <!-- No perdemos el id del producto (name="imagen_actual" por si el usuario no cambia la imagen que conseve la que ya tiene)-->

		<input type="hidden" name="id" value="<?php echo $fila['id'];?>">
		
		<input type="submit" value="Guardar producto" name="modificar" class="btn btn-default">
	</div>
</form>